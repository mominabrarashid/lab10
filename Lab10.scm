(define z 0)
(define a #t)
(define b #f)
(define ADD(lambda (m n)         
		(m s (n s z)) ))
		 
(define SUBTRACT(lambda (m n)         
		(n p m) ))
		
(define AND(lambda (M N)         
		 (N (M a b) b)))
		 
(define OR(lambda (M N)         
		 (N a (M a b))))

(define NOT(lambda (M)         
		 (M b a)))
		 
(define TRUE(lambda (a b)         
		 a ))
		 
(define FALSE(lambda (a b)         
		 b ))

(define LEQ(lambda (m n)       
		(ISZERO (SUBTRACT m n)) ))

(define GEQ(lambda (m n)       
		(ISZERO (SUBTRACT n m)) ))
		 